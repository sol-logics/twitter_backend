-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2022 at 10:04 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rourkee_social`
--

-- --------------------------------------------------------

--
-- Table structure for table `commentlike`
--

CREATE TABLE `commentlike` (
  `likeId` int(11) NOT NULL,
  `commentId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `likeStatus` enum('Like','Unlike') DEFAULT NULL,
  `createdDate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `commentlike`
--

INSERT INTO `commentlike` (`likeId`, `commentId`, `userId`, `likeStatus`, `createdDate`) VALUES
(1, 3, 1, 'Like', '2022-05-02 19:32:08'),
(5, 11, 3, 'Like', '2022-05-04 11:39:55');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `commentId` int(11) NOT NULL,
  `tweetId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `comment` text NOT NULL,
  `commentParentId` int(11) NOT NULL,
  `commentStatus` enum('Enabled','Disabled') DEFAULT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`commentId`, `tweetId`, `userId`, `comment`, `commentParentId`, `commentStatus`, `createdDate`) VALUES
(1, 1, 1, 'Hello! This is my Second tweet..', 0, 'Enabled', '2022-04-28 16:16:27'),
(2, 1, 1, 'Second comment..', 0, 'Enabled', '2022-04-28 16:17:26'),
(3, 1, 1, 'First child comment..', 1, 'Enabled', '2022-04-28 16:17:45'),
(4, 1, 1, 'Second child comment..', 1, 'Enabled', '2022-04-28 16:18:05'),
(5, 1, 2, 'Third child comment..', 3, 'Enabled', '2022-04-28 16:18:23'),
(6, 5, 1, 'First comment..', 0, 'Enabled', '2022-04-28 16:19:10'),
(7, 6, 5, 'First child comment..', 0, 'Enabled', '2022-04-28 16:21:12'),
(8, 6, 5, 'Second child comment..', 0, 'Enabled', '2022-04-28 16:21:52'),
(9, 4, 1, 'Third child comment..', 8, 'Enabled', '2022-04-28 16:22:03'),
(10, 4, 5, 'Third child comment..', 0, 'Enabled', '2022-04-29 14:09:53'),
(11, 6, 1, 'Third child comment YUU..', 0, 'Enabled', '2022-05-02 19:45:29'),
(12, 6, 2, 'Third child comment YUU..', 0, 'Enabled', '2022-05-02 20:04:53'),
(13, 6, 2, 'Third child comment YUU..', 0, 'Enabled', '2022-05-06 13:57:30'),
(14, 6, 2, 'Third child comment YUU..', 0, 'Enabled', '2022-05-06 13:59:12'),
(15, 6, 2, 'Third child comment YUU..', 0, 'Enabled', '2022-05-06 13:59:28'),
(16, 1, 2, 'Third child comment YUU..', 0, 'Enabled', '2022-05-06 13:59:50');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(11) NOT NULL,
  `requestSentBy` int(11) DEFAULT NULL,
  `requestSentTo` int(11) DEFAULT NULL,
  `requestStatus` enum('Pending','Confirmed','Declined','Unfollowed') DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `requestStatusDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `requestSentBy`, `requestSentTo`, `requestStatus`, `createdDate`, `requestStatusDate`) VALUES
(1, 1, 2, 'Confirmed', NULL, '2022-04-26 15:54:27'),
(2, 1, 4, 'Pending', NULL, '2022-04-25 20:04:38'),
(4, 3, 1, 'Pending', NULL, '2022-04-26 15:35:18'),
(7, 4, 3, 'Pending', NULL, '2022-04-26 15:50:35');

-- --------------------------------------------------------

--
-- Table structure for table `retweet`
--

CREATE TABLE `retweet` (
  `id` int(11) NOT NULL,
  `tweetId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `retweet`
--

INSERT INTO `retweet` (`id`, `tweetId`, `userId`, `createdDate`) VALUES
(1, 6, 1, '2022-05-04 17:13:16'),
(18, 9, 1, '2022-05-05 17:03:17');

-- --------------------------------------------------------

--
-- Table structure for table `tweet`
--

CREATE TABLE `tweet` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `content` text DEFAULT NULL,
  `countRetweet` int(11) NOT NULL,
  `countLike` int(11) NOT NULL,
  `countComment` int(11) NOT NULL,
  `createdDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `status` enum('Enabled','Disabled') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tweet`
--

INSERT INTO `tweet` (`id`, `userId`, `content`, `countRetweet`, `countLike`, `countComment`, `createdDate`, `modifiedDate`, `status`) VALUES
(1, 1, 'Hello! This is my first tweet.. which was disabled', 0, 0, 1, '2022-04-26 17:04:36', '2022-04-26 18:00:26', 'Disabled'),
(5, 1, 'Hello! This is my Third tweet..', 0, 2, 3, '2022-04-29 14:03:27', NULL, 'Enabled'),
(6, 1, 'My name is Jeetesh..', 0, 0, 1, '2022-04-29 14:03:40', NULL, 'Enabled'),
(7, 1, 'My name is Jeetesh..', 0, 0, 0, '2022-04-29 17:44:02', NULL, 'Enabled'),
(8, 1, NULL, 0, 0, 0, '2022-05-04 17:12:24', NULL, NULL),
(9, 2, 'Retweet This tweet', 3, 0, 0, '2022-05-04 17:12:50', NULL, 'Enabled');

-- --------------------------------------------------------

--
-- Table structure for table `tweetlike`
--

CREATE TABLE `tweetlike` (
  `likeId` int(11) NOT NULL,
  `tweetId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `likeStatus` enum('Like','Unlike') DEFAULT NULL,
  `createdDate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tweetlike`
--

INSERT INTO `tweetlike` (`likeId`, `tweetId`, `userId`, `likeStatus`, `createdDate`) VALUES
(1, 6, 1, 'Like', '2022-05-02 15:31:46'),
(4, 5, 2, 'Like', '2022-05-02 17:37:19'),
(5, 5, 1, 'Like', '2022-05-05 12:03:17'),
(6, 5, 1, 'Like', '2022-05-06 13:44:18'),
(7, 5, 1, 'Like', '2022-05-06 13:45:02'),
(8, 5, 1, 'Like', '2022-05-06 13:45:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `uuId` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `phoneNumber` varchar(32) DEFAULT NULL,
  `otp` varchar(20) NOT NULL,
  `profileStatus` enum('true','false') NOT NULL DEFAULT 'false',
  `active` tinyint(1) NOT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `modifiedDate` datetime DEFAULT NULL,
  `signupDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuId`, `email`, `password`, `firstName`, `lastName`, `middleName`, `dob`, `phoneNumber`, `otp`, `profileStatus`, `active`, `createdDate`, `modifiedDate`, `signupDate`) VALUES
(1, '43afc17c-81ff-4b07-9930-8d92c676a239', 'jeeteshhajela@gmail.com', '$2b$10$.GBCKue.TXUUYR52FSN04.szpMUq.fqKoRzsek04ltUlwWmxVI93O', 'Jeetesh', 'Hajela', '', '1983-04-03 00:00:00', '9871320189', '123456', 'true', 0, '2022-04-25 19:15:47', '2022-04-26 13:22:53', '2022-04-25 19:16:43'),
(2, '700b45cc-f68a-45fa-a6a1-ec428df65971', 'anshu.s@sollogics.com', '$2b$10$ssBdpt13VNtL8xGHZt3r5.4gcSfJ9uZKsCBAraMaf78kkt0nXVwo.', 'Anshu', 'Porwal', '', '1982-06-18 00:00:00', '9871320175', '123456', 'true', 0, '2022-04-25 19:17:59', '2022-04-25 19:18:09', '2022-04-25 19:18:09'),
(3, 'd366b991-012f-4a13-9fa4-e02a36d8cd8d', 'neerajawasthian@sollogics.com', '$2b$10$uBna60en.c8n8GQjcvA9xuKw6sU0fGarCjgns.HgQIdDxXIwN2VEO', 'Neeraj', 'Awasthi', NULL, '1987-09-21 00:00:00', '8741654127', '123456', 'true', 0, '2022-04-25 19:18:30', '2022-04-25 19:18:44', '2022-04-25 19:18:44'),
(4, '0160402d-0467-4a74-a1e7-a7b454e015de', 'vivekjain@gmail.com', '$2b$10$93oi./VYquUn0M9SB4pYF.QoTI6cJpgMxb7xUIwkJryDRshITf2hS', 'Vivek', 'Jain', 'Kumar', '1976-11-14 00:00:00', '8745213698', '123456', 'true', 0, '2022-04-25 19:19:03', '2022-04-25 19:19:22', '2022-04-25 19:19:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `commentlike`
--
ALTER TABLE `commentlike`
  ADD PRIMARY KEY (`likeId`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`commentId`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retweet`
--
ALTER TABLE `retweet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tweet`
--
ALTER TABLE `tweet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tweetlike`
--
ALTER TABLE `tweetlike`
  ADD PRIMARY KEY (`likeId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `commentlike`
--
ALTER TABLE `commentlike`
  MODIFY `likeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `commentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `retweet`
--
ALTER TABLE `retweet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tweet`
--
ALTER TABLE `tweet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tweetlike`
--
ALTER TABLE `tweetlike`
  MODIFY `likeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

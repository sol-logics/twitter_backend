const express = require('express');
const router = express.Router();
const users = require('../controllers/user.controller.js');
var jwt = require('jsonwebtoken');
const auth = require("../config/auth.js");

// router.post('/signup', async(req, res) => {
//     let userReq = req.body;
//     console.log('--Sign up Post Request--');
//     console.log(userReq);
// })

router.post('/signup', users.signup)
router.post('/verification', users.verification)
router.post('/signin', users.signin)
router.post('/resendOtp', users.resendOtp)
router.put('/users/:id/profile', auth, users.editProfile)
router.get('/users/:id/profiledetail', auth, users.userDetails)
router.put('/users/:id/password', auth, users.changePassword)
router.post('/forgotPassword', users.forgotPassword)
router.post('/otpverification', users.verification)
router.post('/resetPassword', users.forgotPasswordAfterOtpVerification)
router.post('/users/:id/sentRequest', auth, users.sentFollowRequest)
router.get('/users/:id/followers', auth, users.getFollowers)
router.get('/users/:id/following', auth, users.getFollowing)
router.put('/users/:id/requestAction', auth, users.requestAction)
router.delete('/users/:id/delete', auth, users.deleteRequest)
router.get('/users/:id/usersList', users.usersList)
//router.put('/logout', auth, users.logout)
router.post('/users/:id/tweets', auth, users.submitTweet)
router.put('/users/:id/updateTweet', auth, users.updateTweet)
router.get('/users/:id/tweets', auth, users.tweetList)
router.delete('/users/:id/tweets/:tweetId/delete', auth, users.deleteTweet)
router.get('/users/:id/tweets/:tweetId', auth, users.detailTweet)
router.get('/users/:id/tweets/:tweetId/comments', auth, users.tweetCommentList)
router.post('/users/:id/tweets/:tweetId/comments', auth, users.submitTweetComment)
router.post('/users/:id/tweets/:tweetId/like', auth, users.submitTweetLike)
router.get('/users/:id/tweets/:tweetId/like', auth, users.tweetLikeList)
router.post('/users/:id/tweets/:tweetId/comments/:commentId/like', auth, users.submitCommentLike)
router.get('/users/:id/tweets/:tweetId/comments/:commentId/like', auth, users.commentLikeList)
router.delete('/users/:id/tweets/:tweetId/like', auth, users.deleteTweetLike)
router.delete('/users/:id/tweets/:tweetId/comments/:commentId/like', auth, users.deleteCommentLike)
router.post('/users/:id/tweets/:tweetId/retweet', auth, users.submitReTweet)
router.delete('/users/:id/tweets/:tweetId/retweet/:reTweetId/delete', auth, users.deleteReTweet)

module.exports = router;
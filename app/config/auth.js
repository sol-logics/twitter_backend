const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {

    const token = req.body.token || req.query.token || req.headers["x-access-token"];
    if(!token){
        //return res.status(403).send('A token is required for authentication');
        return res.status(404).send({
            status : 404,
            message : 'A token is required for authentication'
        });
    }
    try{
        const decoded = jwt.verify(token, 'Sollogics');
        req.user = decoded;
         if(req.body.requestSentBy){
            if(req.body.requestSentBy != decoded.user_id){
                return res.status(404).send({
                    status : 404,
                    message : 'Logged in user should be same with request sent by user'
                });
            }
        }
    }catch (err){
        return res.status(404).send({
            status : 404,
            message : 'Invalid Token'
        });
    }
    return next();
}

module.exports = verifyToken;
const helperClass = require("./controller.helper");
let helper = new helperClass();

const bcrypt = require("bcrypt");
const saltRounds = 10;
const User = require("../models/user.model.js");
var jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");

//create and save new user
exports.signup = async (req, res) => {

    const isValidEmail = helper.validateEmail(req.body.email);
    if(isValidEmail == "false"){
        res.status(404).send({
            status : 404,
            message : 'Please enter valid email',
        });    
        return;
    }
    //validate request
    if(!req.body.password){
        res.status(404).send({
            status : 404,
            message : 'Password cannot be empty',
        });    
        return;
    }
     
    let createdDate = helper.getCurrentDate();
    let randomNumber = helper.getRandomNumber();

    const password = req.body.password;
    const encryptedPassword = await bcrypt.hash(password, saltRounds)

    // Creating new unique id
    const userId = uuidv4()

    //Create a User
    const user = new User({
        email : req.body.email,
        password : encryptedPassword,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        middleName : req.body.middleName,
        dob : req.body.dob,
        phoneNumber : req.body.phoneNumber,
        otp : randomNumber,
        createdDate : createdDate,
        modifiedDate : '',
        profileStatus : 'false',
        uuId : userId
    })

    //save tutorial in database
    User.signup(user, (err, data) => {
       
        if (err){
            if (err.kind === "user_already_exist"){
                res.status(404).send({
                    status : 404,
                    message : `User already exist with email ${req.body.email}`
                });
            }
            if (err.kind === "user_already_registered_or_not_exist" || err.kind === "user_already_registered"){
                res.status(404).send({
                    status : 404,
                    message : `User already exist with email ${req.body.email}`
                });
            }
        }else{
            
           if(user.query_status === "user_added_success"){
                helper.sendEmailOtpVerification(randomNumber, user.email);
                res.status(200).send({
                    status : 200,
                    message : `User registered successfully`
                })
            }
        }
    });
    return;
  }
  //validate request
  if (!req.body.password) {
    res.status(404).send({
      status: 404,
      message: "Password cannot be empty",
    });
    return;
  }

  let createdDate = helper.getCurrentDate();
  let randomNumber = helper.getRandomNumber();

  const password = req.body.password;
  const encryptedPassword = await bcrypt.hash(password, saltRounds);

  // Creating new unique id
  const userId = uuidv4();

  //Create a User
  const user = new User({
    email: req.body.email,
    password: encryptedPassword,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    middleName: req.body.middleName,
    dob: req.body.dob,
    phoneNumber: req.body.phoneNumber,
    otp: randomNumber,
    createdDate: createdDate,
    modifiedDate: null,
    profileStatus: "false",
    uuId: userId,
  });

  //save tutorial in database
  User.signup(user, (err, data) => {
    if (err) {
      if (err.kind === "user_already_exist") {
        res.status(404).send({
          status: 404,
          message: `User already exist with email ${req.body.email}`,
        });
      }
      if (
        err.kind === "user_already_registered_or_not_exist" ||
        err.kind === "user_already_registered"
      ) {
        res.status(404).send({
          status: 404,
          message: `User already exist with email ${req.body.email}`,
        });
      }
    } else {
      if (user.query_status === "user_added_success") {
        helper.sendEmailOtpVerification(randomNumber);
        res.status(200).send({
          status: 200,
          message: `User registered successfully`,
        });
      }
    }
  });
};

exports.signin = async (req, res) => {
  // const isValidEmail = helper.validateEmail(req.body.email);
  // if (isValidEmail == "false") {
  //   res.status(404).send({
  //     status: 404,
  //     message: "Please enter valid email",
  //   });
  //   return;
  // }
  if (!req.body.password) {
    res.status(404).send({
      status: 404,
      message: "Password cannot be empty",
    });
  }

  const signinObj = {
    email: req.body.email,
    password: req.body.password,
  };

  User.signin(signinObj, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: 404,
          message: `Not Found with this email ${req.body.email}`,
        });
      } else if (err.kind === "user_signin_failure") {
        res.status(404).send({
          status: 404,
          message: "Invalid Username or Password",
        });
      } else if (err.kind === "user_signin_failure_profile_inactive") {
        res.status(404).send({
          status: 404,
          message: "User profile is not active",
        });
      } else if (err.kind === "user_signin_not_exist") {
        res.status(404).send({
          status: 404,
          message: "Username with the given email id not exist",
        });
      } else {
        res.status(500).send({
          status: 500,
          message: `Error retrieving User with email ${req.body.email}`,
        });
      }
    } else {
      if (data.query_status === "user_signin_success") {
        const token = jwt.sign(
          { user_id: data.id, email: data.email },
          "Sollogics",
          {
            expiresIn: "1h",
          }
        );
        data.token = token;

        res.status(200).send({
          status: 200,
          message: "Signin successfully",
          data,
        });
      }
    }
  });
};

exports.resendOtp = (req, res) => {

    const isValidEmail = helper.validateEmail(req.body.email);
    if(isValidEmail == "false"){
        res.status(404).send({
            status : 404,
            message : 'Please enter valid email',
        });    
        return;
    }
    randomNumberOtp = helper.getRandomNumber();
    
    User.resendOtp(req.body, randomNumberOtp, (err, data) => {
         if(err){
            if (err.kind === "user_not_found"){
                res.status(404).send({
                    status : 404,
                    message : `Not Found with this email ${req.body.email}`
                });
            }else if (err.kind === "user_already_verified"){
                res.status(404).send({
                    status : 404,
                    message : `User already verified`
                });
            }else{
                res.status(500).send({
                    status : 500,
                    message : `Error retrieving User with email ${req.body.email}`
                })
            }
        }else{
            if (data.query_status === "user_otp_update_success"){
                helper.sendEmailOtpVerification(randomNumberOtp, req.body.email);
                res.status(200).send({
                    status : 200,
                    message : 'OTP sent to email successfully'
                })
            }
        }
    });
    return;
  }
  randomNumberOtp = helper.getRandomNumber();

  User.resendOtp(req.body, randomNumberOtp, (err, data) => {
    if (err) {
      if (err.kind === "user_not_found") {
        res.status(404).send({
          status: 404,
          message: `Not Found with this email ${req.body.email}`,
        });
      } else if (err.kind === "user_already_verified") {
        res.status(404).send({
          status: 404,
          message: `User already verified`,
        });
      } else {
        res.status(500).send({
          status: 500,
          message: `Error retrieving User with email ${req.body.email}`,
        });
      }
    } else {
      if (data.query_status === "user_otp_update_success") {
        helper.sendEmailOtpVerification(randomNumberOtp);
        res.status(200).send({
          status: 200,
          message: "OTP sent to email successfully",
        });
      }
    }
  });
};

exports.verification = (req, res) => {
  const isValidEmail = helper.validateEmail(req.body.email);
  if (isValidEmail == "false") {
    res.status(404).send({
      status: 404,
      message: "Please enter valid email",
    });
    return;
  }

  if (!req.body.otp) {
    res.status(404).send({
      status: 404,
      message: "OTP cannot be empty",
    });
  }

  const forgotPasswordFlag =
    req.body.forgotPasswordFlag == "true" ? "true" : "false";
  let modifiedDate = helper.getCurrentDate();
  let signupDate = helper.getCurrentDate();

  const changeUserObj = {
    email: req.body.email,
    otp: req.body.otp,
    forgotPasswordFlag: forgotPasswordFlag,
    modifiedDate: modifiedDate,
    signupDate: signupDate,
  };

  User.verification(changeUserObj, (err, data) => {
    if (err) {
      if (err.kind === "user_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid User unique id",
        });
      } else if (err.kind === "user_already_verified") {
        res.status(404).send({
          status: 404,
          message: `User already verified`,
        });
      } else if (err.kind === "otp_not_match") {
        res.status(404).send({
          status: 404,
          message: `Invalid OTP`,
        });
      } else {
        res.status(500).send({
          status: 500,
          message: `Error retrieving User with email ${req.body.email}`,
        });
      }
    } else {
      if (data.query_status === "user_otp_verified") {
        res.status(200).send({
          status: 200,
          message: "User verified successfully",
        });
      }
    }
  });
};

exports.editProfile = (req, res) => {
  const uuid = req.params.id;
  let modifiedDate = helper.getCurrentDate();

  //Create a User
  const user = new User({
    email: req.body.email,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    middleName: req.body.middleName,
    dob: req.body.dob,
    phoneNumber: req.body.phoneNumber,
    modifiedDate: modifiedDate,
    uuId: uuid,
  });

  User.editProfile(user, (err, data) => {
    if (err) {
      res.status(500).send({
        status: 500,
        message: `Invalid User`,
      });
      return;
    } else {
      if (user.query_status === "user_profile_update_success") {
        res.status(200).send({
          status: 200,
          message: "Profile Updated Successfully",
        });
      }
      return;
    }
  });
};

exports.changePassword = async (req, res) => {
  if (req.body.oldPassword.trim() == "") {
    res.status(500).send({
      status: 500,
      message: "Old password cannot be empty",
    });
    return;
  }

  if (req.body.password.trim() == "") {
    res.status(500).send({
      status: 500,
      message: "Password cannot be empty",
    });
    return;
  }

  const encryptedPassword = await bcrypt.hash(req.body.password, saltRounds);
  let modifiedDate = helper.getCurrentDate();

  const changeUserObj = {
    uuId: req.params.id,
    password: encryptedPassword,
    modifiedDate: modifiedDate,
    oldPassword: req.body.oldPassword,
  };

  User.changePassword(changeUserObj, (err, result) => {
    if (err) {
      if (err.kind === "old_password_not_match") {
        res.status(404).send({
          status: 404,
          message: `User Old password do not match`,
        });
      } else {
        res.status(500).send({
          status: 500,
          message: "Invalid User unique id",
        });
      }
    } else {
      if (changeUserObj.query_status === "user_password_update_success") {
        res.status(200).send({
          status: 200,
          message: "Password Updated Successfully",
        });
      }
    }
  });
};

exports.forgotPassword = async (req, res) => {
  const isValidEmail = helper.validateEmail(req.body.email);
  if (isValidEmail == "false") {
    res.status(404).send({
      status: 404,
      message: "Please enter valid email",
    });
    return;
  }
  let randomNumber = helper.getRandomNumber();

  const changeUserObj = {
    email: req.body.email,
    otp: randomNumber,
  };

    User.forgotPassword(changeUserObj, (err, result) => {        
        if(err){
            res.status(500).send({
                status : 500,
                message : `Error retrieving User with email ${req.body.email}`
            });
        }else{
            if(changeUserObj.query_status === "user_otp_update_success"){
                helper.sendEmailOtpVerification(randomNumber, email);
                res.status(200).send({
                    status : 200,
                    message : "OTP Sent on email successfully. Please verify the otp and change your password."
                })
            }
        }
    });     
}

exports.forgotPasswordOtpVerification = async (req, res) => {
  const isValidEmail = helper.validateEmail(req.body.email);
  if (isValidEmail == "false") {
    res.status(404).send({
      status: 404,
      message: "Please enter valid email",
    });
  }

  if (!req.body.otp) {
    res.status(404).send({
      status: 404,
      message: "OTP cannot be empty",
    });
  }

  const changeUserObj = {
    email: req.body.email,
    otp: req.body.otp,
  };

  User.forgotPasswordOtpVerification(changeUserObj, (err, result) => {
    if (err) {
      if (err.kind === "otp_not_match") {
        res.status(404).send({
          status: 404,
          message: `OTP provide with the given email ${req.body.email} not matched`,
        });
      } else {
        res.status(500).send({
          status: 500,
          message: `Error retrieving User with email ${req.body.email}`,
        });
      }
    } else {
      if (changeUserObj.query_status === "user_otp_verified") {
        res.status(200).send({
          status: 200,
          message: "OTP verified successfully",
        });
      }
    }
  });
};

exports.forgotPasswordAfterOtpVerification = async (req, res) => {
  const isValidEmail = helper.validateEmail(req.body.email);
  if (isValidEmail == "false") {
    res.status(404).send({
      status: 404,
      message: "Please enter valid email",
    });
    return;
  }

  if (req.body.password.trim() == "") {
    res.status(500).send({
      status: 500,
      message: "Password cannot be empty",
    });
    return;
  }

  const encryptedPassword = await bcrypt.hash(req.body.password, saltRounds);
  let modifiedDate = helper.getCurrentDate();

  const changeUserObj = {
    email: req.body.email,
    password: encryptedPassword,
    modifiedDate: modifiedDate,
  };

  User.forgotPasswordAfterOtpVerification(changeUserObj, (err, result) => {
    if (err) {
      if (err.kind === "old_password_not_match") {
        res.status(404).send({
          status: 404,
          message: `User Old password do not match`,
        });
      } else {
        res.status(500).send({
          status: 500,
          message: `Error retrieving User with email ${req.body.email}`,
        });
      }
    } else {
      if (changeUserObj.query_status === "user_password_update_success") {
        res.status(200).send({
          status: 200,
          message: "Password Updated Successfully",
        });
      }
    }
  });
};

exports.sentFollowRequest = async (req, res) => {
  const requestSentTo = req.body.requestSentTo;

  if (!requestSentTo) {
    res.status(404).send({
      status: 404,
      message: "User receiving the request cannot be empty",
    });
    return;
  }
  let createdDate = helper.getCurrentDate();

  const followObj = {
    uuId: req.params.id,
    requestSentTo: requestSentTo,
    requestStatus: "Pending",
    requestStatusDate: createdDate,
  };

  User.sentFollowRequest(followObj, (err, result) => {
    if (err) {
      if (err.kind === "request_sent_by_not_match") {
        res.status(404).send({
          status: 404,
          message: "Invalid user unique id",
        });
        return;
      }
      if (err.kind === "request_sent_to_not_match") {
        res.status(404).send({
          status: 404,
          message: "Request sent to User doesn't exist",
        });
        return;
      }
      if (err.kind === "request_already_exist") {
        res.status(404).send({
          status: 404,
          message: "Request already exist",
        });
        return;
      }
    } else {
      if (followObj.query_status === "request_sent_success") {
        res.status(200).send({
          status: 200,
          message: "Request sent successfully",
        });
        return;
      }
    }
  });
};

exports.getFollowers = async (req, res) => {
  const uuId = req.params.id;

  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }
  const requestStatus = req.query.requestStatus;
  if (requestStatus == "") {
    res.status(404).send({
      status: 404,
      message: "Request Status cannot be empty",
    });
    return;
  }

  const countFlag = "true" ? req.query.countFlag == "true" : "false";

  const followerObj = {
    uuId: uuId,
    countFlag: countFlag,
    requestStatus: requestStatus,
  };

  User.getFollowers(followerObj, (err, followerData) => {
    if (err) {
      if (err.kind === "user_not_match") {
        res.status(404).send({
          status: 404,
          message: "Logged in User not exist",
        });
        return;
      }
    } else {
      if (countFlag === false) {
        let followerRow = [];
        Object.keys(followerData).forEach(function (key, index) {
          followerRow[index] = {
            id: followerData[key].requestSentTo,
            firstName: followerData[key].firstName,
            lastName: followerData[key].lastName,
            middleName: followerData[key].middleName,
            dob: followerData[key].dob,
            phoneNumber: followerData[key].phoneNumber,
            email: followerData[key].email,
            uuId: followerData[key].uuId,
            id: followerData[key].id,
          };
        });

        res.status(200).send({
          status: 200,
          message: "List of followers",
          data: followerRow,
        });
      } else {
        res.status(200).send({
          status: 200,
          message: "Count of followers",
          data: [],
          count: followerData[0].count,
        });
      }
    }
  });
};

exports.getFollowing = async (req, res) => {
  const uuId = req.params.id;
  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }

  const requestStatus = req.query.requestStatus;
  if (requestStatus == "") {
    res.status(404).send({
      status: 404,
      message: "Request Status cannot be empty",
    });
    return;
  }
  const countFlag = "true" ? req.query.countFlag == "true" : "false";

  const followingObj = {
    uuId: uuId,
    countFlag: countFlag,
    requestStatus: requestStatus,
  };

  User.getFollowing(followingObj, (err, followingData) => {
    if (err) {
      if (err.kind === "user_not_match") {
        res.status(404).send({
          status: 404,
          message: "Logged in User not exist",
        });
        return;
      }
    } else {
      if (countFlag === false) {
        let followingRow = [];
        Object.keys(followingData).forEach(function (key, index) {
          followingRow[index] = {
            id: followingData[key].requestSentTo,
            firstName: followingData[key].firstName,
            lastName: followingData[key].lastName,
            middleName: followingData[key].middleName,
            dob: followingData[key].dob,
            phoneNumber: followingData[key].phoneNumber,
            email: followingData[key].email,
            uuId: followingData[key].uuId,
            id: followingData[key].id,
          };
        });

        res.status(200).send({
          status: 200,
          message: "List of following",
          data: followingRow,
        });
      } else {
        res.status(200).send({
          status: 200,
          message: "Count of following",
          data: [],
          count: followingData[0].count,
        });
      }
    }
  });
};

exports.requestAction = async (req, res) => {
  const uuId = req.params.id;
  const requestSentBy = req.body.memberId;
  const requestStatus = req.body.requestStatus;
  const requestStatusDate = helper.getCurrentDate();

  // if(requestSentBy.trim() == ""){
  //     res.status(404).send({
  //         status : 404,
  //         message : 'User receiving the follower request cannot be empty'
  //     })
  //     return;
  // }
  statusArray = ["", "Pending"];
  if (statusArray.includes(requestStatus.trim())) {
    res.status(404).send({
      status: 404,
      message: "Please send the correct request status",
    });
    return;
  }

  const requestObj = {
    uuId: uuId,
    requestSentBy: requestSentBy,
    requestStatus: requestStatus,
    requestStatusDate: requestStatusDate,
  };

  User.requestAction(requestObj, (err, requestData) => {
    if (err) {
      //if(err.kind === "request_delete_to_not_match"){
      res.status(404).send({
        status: 404,
        message: err,
      });
      return;
      //}
    } else {
      res.status(200).send({
        status: 200,
        message: "Request Cofirmation",
        data: requestData,
      });
    }
  });
};

exports.deleteRequest = async (req, res) => {
  const uuId = req.params.id;
  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }

  const requestSentTo = req.query.requestSentTo;
  if (!requestSentTo) {
    res.status(404).send({
      status: 404,
      message: "User receiving the request cannot be empty",
    });
    return;
  }

  const followObj = {
    requestSentTo: requestSentTo,
    uuId: uuId,
  };

  User.deleteRequest(followObj, (err, result) => {
    if (err) {
      if (err.kind === "request_delete_to_not_match") {
        res.status(404).send({
          status: 404,
          message: "Delete Request sent by User not exist",
        });
        return;
      }
      if (err.kind === "request_delete_to_not_match") {
        res.status(404).send({
          status: 404,
          message: "Delete Request sent to User not exist",
        });
        return;
      }
    } else {
      if (followObj.query_status === "delete_request_sent_success") {
        res.status(200).send({
          status: 200,
          message: "Delete Request successfully",
        });
        return;
      }
    }
  });
};

exports.userDetails = async (req, res) => {
  const uuId = req.params.id;

  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }

  User.userDetails(uuId, (err, userDetails) => {
    if (err) {
      if (err.kind === "user_not_match") {
        res.status(404).send({
          status: 404,
          message: "Logged in User not exist",
        });
        return;
      }
    } else {
      let userDetailRow = [];

      Object.keys(userDetails).forEach(function (key, index) {
        userDetailRow[index] = {
          id: userDetails[key].id,
          uuId: userDetails[key].uuId,
          firstName: userDetails[key].firstName,
          lastName: userDetails[key].lastName,
          middleName: userDetails[key].middleName,
          dob: userDetails[key].dob,
          phoneNumber: userDetails[key].phoneNumber,
          email: userDetails[key].email,
        };
      });

      res.status(200).send({
        status: 200,
        message: "User Details",
        data: userDetailRow,
      });
    }
  });
};

exports.logout = async (req, res) => {
  const token = jwt.sign({ user_id: data.id, email: data.email }, "", {
    expiresIn: "1",
  });
  res.status(200).send({
    status: 200,
    message: "Logout successfully",
  });
  return;
};

exports.usersList = async (req, res) => {
  const uuId = req.params.id;
  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }
  const searchUserString = req.query.searchUserString;
  if (searchUserString == "") {
    res.status(404).send({
      status: 404,
      message: "Search String cannot be empty and more than 3 letters",
    });
    return;
  }

  const countFlag = "true" ? req.query.countFlag == "true" : "false";

  const searchObj = {
    uuId: uuId,
    searchUserString: searchUserString,
    countFlag: countFlag,
  };

  User.getUsersList(searchObj, (err, searchData) => {
    if (err) {
      if (err.kind === "user_not_match") {
        res.status(404).send({
          status: 404,
          message: "Logged in User not exist",
        });
        return;
      }
    } else {
      if (countFlag === false) {
        let userRow = [];
        Object.keys(searchData).forEach(function (key, index) {
          userRow[index] = {
            id: searchData[key].id,
            firstName: searchData[key].firstName,
            lastName: searchData[key].lastName,
            middleName: searchData[key].middleName,
            followType: searchData[key].followerTypeFlag,
          };
        });

        res.status(200).send({
          status: 200,
          message: "List of Users",
          data: userRow,
        });
      } else {
        res.status(200).send({
          status: 200,
          message: "Count of Users",
          data: [],
          count: searchData[0].countUser,
        });
      }
    }
  });
};

exports.submitTweet = (req, res) => {
  const uuId = req.params.id;
  let createdDate = helper.getCurrentDate();

  //Create a User
  const tweetObj = {
    uuId: uuId,
    content: req.body.content,
    createdDate: createdDate,
    status: req.body.status,
  };

  User.submitTweet(tweetObj, (err, data) => {
    if (err) {
      res.status(500).send({
        status: 500,
        message: err,
      });
      return;
    } else {
      if (data.query_status === "tweet_insert_success") {
        res.status(200).send({
          status: 200,
          message: "Tweet Added Successfully",
        });
      }
      return;
    }
  });
};

exports.updateTweet = (req, res) => {
  const uuId = req.params.id;
  let modifiedDate = helper.getCurrentDate();

  //Create a User
  const tweetObj = {
    uuId: uuId,
    tweetId: req.body.tweetId,
    content: req.body.content,
    modifiedDate: modifiedDate,
    status: req.body.status,
  };

  User.updateTweet(tweetObj, (err, data) => {
    if (err) {
      if (err.kind === "tweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet",
        });
      }
    } else {
      if (data.query_status === "tweet_update_success") {
        res.status(200).send({
          status: 200,
          message: "Tweet Update Successfully",
        });
      }
      return;
    }
  });
};

exports.tweetList = async (req, res) => {
  const uuId = req.params.id;
  const tweetTab = req.query.tweetTab;

  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }
  if (!tweetTab) {
    res.status(404).send({
      status: 404,
      message: "Please verify the type of tweet",
    });
    return;
  }

  const countFlag = "true" ? req.query.countFlag == "true" : "false";

  const tweetObj = {
    uuId: uuId,
    countFlag: countFlag,
    status: req.query.status,
    tweetTab: tweetTab,
  };
console.log('Jeetesh');
    User.getReTweetListOfFollower(tweetObj, (err, tweetDataFollower) => {
        tweetObj.reTweetUserId =  tweetDataFollower;
      
        User.getTweetList(tweetObj, (err, tweetData) => {
          console.log('Sollll', err);
            if (err) {
              if (err.kind === "user_not_match") {
                res.status(404).send({
                  status: 404,
                  message: "Logged in User not exist",
                });
                return;
              }
              if (err.kind === "tweet_not_found") {
                res.status(404).send({
                  status: 404,
                  message: "Invalid Tweet",
                });
                return;
              }
            } else {
                
              if (countFlag === false) {
                
                let tweetRow = [];
                Object.keys(tweetData).forEach(function (key, index) {
                  tweetRow[index] = {
                    id: tweetData[key].id,
                    content: tweetData[key].content,
                    createdDate: tweetData[key].createdDate,
                    countRetweet: tweetData[key].countRetweet,
                    countLike: tweetData[key].countLike,
                    countComment: tweetData[key].countComment,
                    likeFlag: tweetData[key].likeFlag,
                    firstName: tweetData[key].firstName,
                    lastName: tweetData[key].lastName,
                  };
                });
        
                res.status(200).send({
                  status: 200,
                  message: "List of Tweet",
                  count: tweetRow.length,
                  data: tweetRow,
                });
              } else {
                res.status(200).send({
                  status: 200,
                  message: "Count of Tweet",
                  data: [],
                  count: tweetData[0].count,
                });
              }
            }
          });
    })
  
};

exports.deleteTweet = async (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;

  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }

  const tweetObj = {
    tweetId: tweetId,
    uuId: uuId,
  };

  User.deleteTweet(tweetObj, (err, result) => {
    if (err) {
      if (err.kind === "tweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet",
        });
        return;
      }
      if (err.kind === "user_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid User",
        });
        return;
      }
    } else {
      if (tweetObj.query_status === "tweet_delete_success") {
        res.status(200).send({
          status: 200,
          message: "Delete Request successfully",
        });
        return;
      }
    }
  });
};

exports.tweetCommentList = async (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  const commentStatus = req.query.commentStatus;
  const commentParentId = req.query.commentParentId;
  const countFlag = "true" ? req.query.countFlag == "true" : "false";

  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }
  if (!commentStatus) {
    res.status(404).send({
      status: 404,
      message: "Comment status cannot be empty",
    });
    return;
  }

  const tweetCommentObj = {
    uuId: uuId,
    countFlag: countFlag,
    tweetId: tweetId,
    commentStatus: commentStatus,
    commentParentId: commentParentId,
  };

  User.getTweetCommentList(tweetCommentObj, (err, tweetCommentData) => {
    if (err) {
      if (err.kind === "user_not_match") {
        res.status(404).send({
          status: 404,
          message: "Logged in User not exist",
        });
        return;
      }
      if (err.kind === "tweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet or comment",
        });
        return;
      }
    } else {
      if (countFlag === false) {
        let tweetCommentRow = [];
        Object.keys(tweetCommentData).forEach(function (key, index) {
          tweetCommentRow[index] = {
            commentId: tweetCommentData[key].commentId,
            commentParentId: tweetCommentData[key].commentParentId,
            tweetId: tweetCommentData[key].tweetId,
            comment: tweetCommentData[key].comment,
            createdDate: tweetCommentData[key].createdDate,
          };
        });

        res.status(200).send({
          status: 200,
          message: "List of Comments",
          data: tweetCommentRow,
          count: tweetCommentRow.length,
        });
      } else {
        res.status(200).send({
          status: 200,
          message: "Count of Comments",
          data: [],
          count: tweetCommentData[0].count,
        });
      }
    }
  });
};

exports.submitTweetComment = (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  let createdDate = helper.getCurrentDate();

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }
  if (!req.body.comment) {
    res.status(404).send({
      status: 404,
      message: "Comment cannot be empty",
    });
    return;
  }
  if (!req.body.commentStatus) {
    res.status(404).send({
      status: 404,
      message: "Comment status cannot be empty",
    });
    return;
  }

  //Create a User
  const tweetCommentObj = {
    uuId: uuId,
    tweetId: tweetId,
    comment: req.body.comment,
    commentParentId: req.body.commentParentId,
    createdDate: createdDate,
    commentStatus: req.body.commentStatus,
  };

  User.submitTweetComment(tweetCommentObj, (err, data) => {
    if (err) {
      res.status(500).send({
        status: 500,
        message: err,
      });
      if (err.kind === "tweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet or comment",
        });
        return;
      }
      return;
    } else {
      if (data.query_status === "tweet_comment_insert_success") {
        res.status(200).send({
          status: 200,
          message: "Comment Added Successfully",
        });
      }
      return;
    }
  });
};

exports.submitTweetLike = (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  let createdDate = helper.getCurrentDate();

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }

  //Create a User
  const tweetLikeObj = {
    uuId: uuId,
    tweetId: tweetId,
    createdDate: createdDate,
  };

  User.submitTweetLike(tweetLikeObj, (err, data) => {
    if (err) {
      res.status(500).send({
        status: 500,
        message: err,
      });
      return;
    } else {
      if (data.query_status === "tweet_like_insert_success") {
        res.status(200).send({
          status: 200,
          message: "Like Added Successfully",
        });
      }
      return;
    }
  });
};

exports.tweetLikeList = async (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  const countFlag = "true" ? req.query.countFlag == "true" : "false";

  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }

  const tweetLikeObj = {
    uuId: uuId,
    countFlag: countFlag,
    tweetId: tweetId,
  };

  User.getTweetLikeList(tweetLikeObj, (err, tweetLikeData) => {
    if (err) {
      if (err.kind === "user_not_match") {
        res.status(404).send({
          status: 404,
          message: "Logged in User not exist",
        });
        return;
      }
      if (err.kind === "tweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet or comment",
        });
        return;
      }
    } else {
      if (countFlag === false) {
        let tweetLikeRow = [];
        Object.keys(tweetLikeData).forEach(function (key, index) {
          tweetLikeRow[index] = {
            likeId: tweetLikeData[key].likeId,
            tweetId: tweetLikeData[key].tweetId,
            userId: tweetLikeData[key].userId,
            likeStatus: tweetLikeData[key].likeStatus,
            createdDate: tweetLikeData[key].createdDate,
          };
        });

        res.status(200).send({
          status: 200,
          message: "List of Like",
          count: tweetLikeRow.length,
          data: tweetLikeRow,
        });
      } else {
        res.status(200).send({
          status: 200,
          message: "Count of Like",
          count: tweetLikeData[0].count,
          data: [],
        });
      }
    }
  });
};

exports.submitCommentLike = (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  const commentId = req.params.commentId;
  let createdDate = helper.getCurrentDate();

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }
  if (!commentId) {
    res.status(404).send({
      status: 404,
      message: "Comment id cannot be empty",
    });
    return;
  }

  //Create a User
  const tweetLikeObj = {
    uuId: uuId,
    tweetId: tweetId,
    commentId: commentId,
    createdDate: createdDate,
  };

  User.submitCommentLike(tweetLikeObj, (err, data) => {
    if (err) {
      if (err.kind === "comment_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet or comment",
        });
        return;
      }
      return;
    } else {
      if (data.query_status === "comment_like_insert_success") {
        res.status(200).send({
          status: 200,
          message: "Like Added Successfully",
        });
      }
      return;
    }
  });
};

exports.commentLikeList = async (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  const commentId = req.params.commentId;
  const countFlag = "true" ? req.query.countFlag == "true" : "false";

  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }

  const tweetLikeObj = {
    uuId: uuId,
    countFlag: countFlag,
    tweetId: tweetId,
    commentId: commentId,
  };
  console.log(tweetLikeObj);

  User.getCommentLikeList(tweetLikeObj, (err, tweetLikeData) => {
    if (err) {
      if (err.kind === "user_not_match") {
        res.status(404).send({
          status: 404,
          message: "Logged in User not exist",
        });
        return;
      }
      if (err.kind === "tweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet or comment",
        });
        return;
      }
    } else {
      if (countFlag === false) {
        let tweetLikeRow = [];
        Object.keys(tweetLikeData).forEach(function (key, index) {
          tweetLikeRow[index] = {
            likeId: tweetLikeData[key].likeId,
            tweetId: tweetLikeData[key].tweetId,
            userId: tweetLikeData[key].userId,
            likeStatus: tweetLikeData[key].likeStatus,
            createdDate: tweetLikeData[key].createdDate,
          };
        });

        res.status(200).send({
          status: 200,
          message: "List of Like",
          count: tweetLikeRow.length,
          data: tweetLikeRow,
        });
      } else {
        res.status(200).send({
          status: 200,
          message: "Count of Like",
          count: tweetLikeData[0].count,
          data: [],
        });
      }
    }
  });
};

exports.deleteTweetLike = (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  console.log("thins is tweetid  ", tweetId);
  console.log("thins is uuid  ", uuId);

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }

  //Create a Tweet Obj
  const tweetLikeObj = {
    uuId: uuId,
    tweetId: tweetId,
  };

  User.deleteTweetLike(tweetLikeObj, (err, data) => {
    if (err) {
      if (err.kind === "tweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet or comment",
        });
        return;
      }
    } else {
      if (data.query_status === "tweet_unlike_success") {
        res.status(200).send({
          status: 200,
          message: "Unlike Successfully",
        });
      }
      return;
    }
  });
};

exports.deleteCommentLike = (req, res) => {
  console.log("this method hits ", req, res);
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  const commentId = req.params.commentId;

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }

  if (!commentId) {
    res.status(404).send({
      status: 404,
      message: "Comment id cannot be empty",
    });
    return;
  }

  //Create a Tweet Obj
  const tweetLikeObj = {
    uuId: uuId,
    tweetId: tweetId,
    commentId: commentId,
  };

  User.deleteCommentLike(tweetLikeObj, (err, data) => {
    if (err) {
      if (err.kind === "tweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet or comment",
        });
        return;
      }
    } else {
      if (data.query_status === "tweet_unlike_success") {
        res.status(200).send({
          status: 200,
          message: "Unlike Successfully",
        });
      }
      return;
    }
  });
};

exports.submitReTweet = (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  let createdDate = helper.getCurrentDate();

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }

  //Create a User
  const tweetObj = {
    uuId: uuId,
    createdDate: createdDate,
    tweetId: tweetId,
  };

  User.submitReTweet(tweetObj, (err, data) => {
    if (err) {
      if (err.kind === "user_not_found") {
        res.status(404).send({
          status: 404,
          message: "User not found",
        });
        return;
      }
    } else {
      if (data.query_status === "retweet_insert_success") {
        res.status(200).send({
          status: 200,
          message: "Re-Tweet Successfully",
        });
      }
      return;
    }
  });
};

exports.deleteReTweet = (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;
  const reTweetId = req.params.reTweetId;

  if (!tweetId) {
    res.status(404).send({
      status: 404,
      message: "Tweet id cannot be empty",
    });
    return;
  }

  if (!reTweetId) {
    res.status(404).send({
      status: 404,
      message: "Retweet id cannot be empty",
    });
    return;
  }

  //Create a User
  const tweetObj = {
    uuId: uuId,
    tweetId: tweetId,
    reTweetId: reTweetId,
  };

  User.deleteReTweet(tweetObj, (err, data) => {
    if (err) {
      if (err.kind === "user_not_found") {
        res.status(404).send({
          status: 404,
          message: "User not found",
        });
        return;
      }
      if (err.kind === "retweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet",
        });
        return;
      }
    } else {
      if (data.query_status === "retweet_delete_success") {
        res.status(200).send({
          status: 200,
          message: "Re-Tweet Deleted Successfully",
        });
      }
      return;
    }
  });
};

exports.detailTweet = async (req, res) => {
  const uuId = req.params.id;
  const tweetId = req.params.tweetId;

  if (!uuId) {
    res.status(404).send({
      status: 404,
      message: "Logged in user cannot be empty",
    });
    return;
  }

  const tweetObj = {
    tweetId: tweetId,
    uuId: uuId,
  };

  User.detailTweet(tweetObj, (err, tweetDetails) => {
    if (err) {
      if (err.kind === "tweet_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid Tweet",
        });
        return;
      }
      if (err.kind === "user_not_found") {
        res.status(404).send({
          status: 404,
          message: "Invalid User",
        });
        return;
      }
    } else {
      let tweetDetailRow = [];

      Object.keys(tweetDetails).forEach(function (key, index) {
        tweetDetailRow[index] = {
          id: tweetDetails[key].id,
          content: tweetDetails[key].content,
          countRetweet: tweetDetails[key].countRetweet,
          countLike: tweetDetails[key].countLike,
          countComment: tweetDetails[key].countComment,
          createdDate: tweetDetails[key].createdDate,
          uuId: tweetDetails[key].uuId,
          userName: tweetDetails[key].userName,
          likeUsersListCount: tweetDetails[key].likeUsersList.length,
          likeUsersList: tweetDetails[key].likeUsersList,
          commentListCount: tweetDetails[key].commentList.length,
          commentList: tweetDetails[key].commentList,
        };
      });

      res.status(200).send({
        status: 200,
        message: "Tweet Details",
        data: tweetDetailRow,
      });
      return;
    }
  });
};

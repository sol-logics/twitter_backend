class Helper{

    getCurrentDate = () => {
        //date time function to be moved in another helper files afterwards
        let date_ob = new Date();

        // current date
        // adjust 0 before single digit date
        let date = ("0" + date_ob.getDate()).slice(-2);
        
        // current month
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        
        // current year
        let year = date_ob.getFullYear();
        
        // current hours
        let hours = date_ob.getHours();
        
        // current minutes
        let minutes = date_ob.getMinutes();
        
        // current seconds
        let seconds = date_ob.getSeconds();
        
        // prints date in YYYY-MM-DD format
        console.log(year + "-" + month + "-" + date);
        
        // prints date & time in YYYY-MM-DD HH:MM:SS format
        let currentDate = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

        return currentDate;
    } 

    getRandomNumber = () => {

        //const randomNumber = Math.floor(100000 + Math.random() * 900000);
        const randomNumber = '123456';
        return randomNumber;
    }

    validateEmail = (email) => {

        let validFlag = "true";
        const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

        const isValidEmail = emailRegexp.test(email);
        if(!isValidEmail){
            validFlag = "false";
        }
        return validFlag;
    }

    sendEmailOtpVerification = (otp, email) => {
        	
		const nodemailer = require('nodemailer'); 
		let mailTransporter = nodemailer.createTransport({
		    service: 'gmail',
		    auth: {
		        user: 'pragya.patal@gmail.com',
		        pass: 'pragya.patal@gmail.com'
		    }
		});
		
		let bodyMessage = 'Hello User,<br/><p>Please use this verification code to verify your registration on our app.</p>';
            bodyMessage += '<h3>' + otp + '</h3>';
            bodyMessage += '<p>If you didn\'t request this,you can ignore this email or let us know</p>';
            bodyMessage += '<br/><p>Thanks!</p>';
            bodyMessage += '<p>Rourkee Social</p>';
		 
		let mailDetails = {
		    from: 'Rourkee Social <pragya.patal@gmail.com>', // sender address
                to: email, // list of receivers
                subject: "OTP Verification for registered user", // Subject line
                text: "Rourkee Social", // plain text body
                html: bodyMessage, // html body
		};
		 
		mailTransporter.sendMail(mailDetails, function(err, data) {
		    if(err) {
		        console.log(err);
		    } else {
		        console.log('Email sent successfully');
		    }
		});
		
        //main().catch(console.error);
    }
}

module.exports = Helper;
const bcrypt = require("bcrypt");
const saltRounds = 10;
const sql = require("./db.js");

//constructor
const User = function (user) {
  this.id = user.id;
  this.uuId = user.uuId;
  this.email = user.email;
  this.password = user.password;
  this.firstName = user.firstName;
  this.lastName = user.lastName;
  this.middleName = user.middleName;
  this.dob = user.dob;
  this.phoneNumber = user.phoneNumber;
  this.otp = user.otp;
  this.createdDate = user.createdDate;
  this.modifiedDate = user.modifiedDate;
  this.profileStatus = user.profileStatus;
};

User.signup = (newUser, result) => {
  sql.query(
    `SELECT * FROM users WHERE email = ?`,
    [newUser.email],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        sql.query(
          `SELECT * FROM users WHERE email = ? && profileStatus = ?`,
          [newUser.email, "false"],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              sql.query(
                "UPDATE users SET uuId = ?, password = ?, otp = ?, firstName, lastName WHERE email = ?",
                [
                  newUser.uuId,
                  newUser.password,
                  newUser.otp,
                  newUser.email,
                  newUser.firstName,
                  newUser.lastName,
                ],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  newUser.id = res.insertId;
                  newUser.query_status = "user_added_success";
                  result(null, newUser);
                }
              );
            } else {
              result({ kind: "user_already_registered" }, null);
            }
          }
        );
      } else {
        sql.query("INSERT INTO users SET ?", [newUser], (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
          newUser.id = res.insertId;
          newUser.query_status = "user_added_success";
          result(null, newUser);
        });
      }
    }
  );
};

User.verification = (user, result) => {
  sql.query(`SELECT * FROM users WHERE email = ?`, [user.email], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      // this condition is only for the user when otp is verified successfully
      //at the time of registration. forgotPasswordFlag true will send in case of forgot password
      if (user.forgotPasswordFlag == "false") {
        if (res[0].profileStatus !== "false") {
          result({ kind: "user_already_verified" }, null);
          return;
        }
      }
      if (res[0].otp !== user.otp) {
        result({ kind: "otp_not_match" }, null);
        return;
      }
      let signupDate = res[0].signupDate;

      if (res[0].signupDate === null) {
        signupDate = user.signupDate;
      }
      sql.query(
        "UPDATE users SET profileStatus = ?, modifiedDate = ?, signupDate = ? WHERE email = ? && otp = ?",
        ["true", user.modifiedDate, signupDate, user.email, user.otp],
        (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
          /*if (res.affectedRows == 0) {
                    // not found user with the user id
                    result(null, {email: user.email, ...user});
                    return;
                }*/
          user.query_status = "user_otp_verified";
          result(null, user);
          return;
        }
      );
    } else {
      result({ kind: "user_not_found" }, null);
    }
  });
};

User.signin = (user, result) => {
  sql.query(
    `SELECT * FROM users WHERE email = ? || phoneNumber = ?`,
    [user.email, user.email],
    async (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        if (res[0].profileStatus == "true") {
          const comparison = await bcrypt.compare(
            user.password,
            res[0].password
          );
          if (comparison) {
            //delete those elements which we don't need in our array
            delete res[0].password;
            delete res[0].otp;
            delete res[0].profileStatus;
            delete res[0].active;
            delete res[0].createdDate;
            delete res[0].modifiedDate;
            delete res[0].signupDate;
            //delete code ends

            userData = res[0];
            userData.query_status = "user_signin_success";
            result(null, userData);
            return;
          } else {
            result({ kind: "user_signin_failure" }, null);
            return;
          }
        } else {
          result({ kind: "user_signin_failure_profile_inactive" }, null);
          return;
        }
      } else {
        result({ kind: "user_signin_not_exist" }, null);
        return;
      }

      //not found user with the user id
      result({ kind: "not_found" }, null);
    }
  );
};

//resend OTP functionality
User.resendOtp = (userInput, otp, result) => {
  sql.query(
    `SELECT * FROM users WHERE email = ?`,
    [userInput.email],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        if (res[0].profileStatus == "true") {
          result({ kind: "user_already_verified" }, null);
          return;
        }

        sql.query(
          "UPDATE users SET otp = ? WHERE email = ?",
          [otp, userInput.email],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }

            userInput.query_status = "user_otp_update_success";
            result(null, userInput);
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
        return;
      }
    }
  );
};

User.editProfile = (user, result) => {
  sql.query(`SELECT * FROM users WHERE uuId = ?`, [user.uuId], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      sql.query(
        "UPDATE users SET firstName = ?, lastName = ?, middleName = ?, dob = ?, phoneNumber = ? WHERE uuId = ?",
        [
          user.firstName,
          user.lastName,
          user.middleName,
          user.dob,
          user.phoneNumber,
          user.uuId,
        ],
        (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
          user.query_status = "user_profile_update_success";
          result(null, user);
          return;
        }
      );
    } else {
      result({ kind: "user_not_found" }, null);
    }
  });
};

User.changePassword = (user, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [user.uuId],
    async (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        const comparison = await bcrypt.compare(
          user.oldPassword,
          res[0].password
        );
        if (comparison) {
          sql.query(
            "UPDATE users SET password = ?, modifiedDate = ? WHERE uuId = ?",
            [user.password, user.modifiedDate, user.uuId],
            (err, res) => {
              if (err) {
                console.log("error : ", err);
                result(err, null);
                return;
              }
              user.query_status = "user_password_update_success";
              result(null, user);
              return;
            }
          );
        } else {
          result({ kind: "old_password_not_match" }, null);
          return;
        }
      } else {
        result({ kind: "user_not_found" }, null);
        return;
      }
    }
  );
};

User.forgotPassword = (user, result) => {
  sql.query(`SELECT * FROM users WHERE email = ?`, [user.email], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      sql.query(
        "UPDATE users SET otp = ? WHERE email = ?",
        [user.otp, user.email],
        (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }

          user.query_status = "user_otp_update_success";
          result(null, user);
        }
      );
    } else {
      result({ kind: "user_not_found" }, null);
      return;
    }
  });
};

User.forgotPasswordOtpVerification = (user, result) => {
  sql.query(`SELECT * FROM users WHERE email = ?`, [user.email], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      if (res[0].otp !== user.otp) {
        result({ kind: "otp_not_match" }, null);
        return;
      }
      user.query_status = "user_otp_verified";
      result(null, user);
    }
  });
};

User.forgotPasswordAfterOtpVerification = (user, result) => {
  sql.query(
    `SELECT * FROM users WHERE email = ?`,
    [user.email],
    async (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        sql.query(
          "UPDATE users SET password = ?, modifiedDate = ? WHERE email = ?",
          [user.password, user.modifiedDate, user.email],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            user.query_status = "user_password_update_success";
            result(null, user);
            return;
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
        return;
      }
    }
  );
};

User.sentFollowRequest = (follow, result) => {
  sql.query(`SELECT * FROM users WHERE uuId = ?`, [follow.uuId], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      const loggedInUser = res[0];

      sql.query(
        `SELECT * FROM users WHERE id = ?`,
        [follow.requestSentTo],
        (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
          if (res.length) {
            sql.query(
              "SELECT * FROM followers WHERE requestSentBy = ? && requestSentTo = ?",
              [loggedInUser.id, follow.requestSentTo],
              (err, res) => {
                if (err) {
                  console.log("error : ", err);
                  result(err, null);
                  return;
                }
                if (res.length) {
                  result({ kind: "request_already_exist" }, null);
                } else {
                  sql.query(
                    "INSERT INTO followers SET requestSentBy = ?, requestSentTo = ?, requestStatus = ?, requestStatusDate = ?",
                    [
                      loggedInUser.id,
                      follow.requestSentTo,
                      follow.requestStatus,
                      follow.requestStatusDate,
                    ],
                    (err, res) => {
                      if (err) {
                        console.log("error : ", err);
                        result(err, null);
                        return;
                      }
                      follow.query_status = "request_sent_success";
                      result(null, follow);
                    }
                  );
                }
              }
            );
          } else {
            result({ kind: "request_sent_to_not_match" }, null);
            return;
          }
        }
      );
    } else {
      result({ kind: "request_sent_by_not_match" }, null);
      return;
    }
  });
};

User.getFollowers = (followObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [followObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        if (followObj.countFlag === false) {
          sql.query(
            `SELECT u.* FROM followers as f INNER JOIN users as u ON f.requestSentBy = u.id WHERE f.requestSentTo = ? && f.requestStatus = ?`,
            [res[0].id, followObj.requestStatus],
            (err, res) => {
              if (err) {
                console.log("error : ", err);
                result(err, null);
                return;
              }
              result(null, res);
              return;
            }
          );
        } else {
          sql.query(
            `SELECT count(f.id) as count FROM followers as f INNER JOIN users as u ON f.requestSentBy = u.id WHERE f.requestSentTo = ? && f.requestStatus = ?`,
            [res[0].id, followObj.requestStatus],
            (err, res) => {
              if (err) {
                console.log("error : ", err);
                result(err, null);
                return;
              }
              result(null, res);
              return;
            }
          );
        }
      } else {
        result({ kind: "user_not_match" }, null);
        return;
      }
    }
  );
};

User.getFollowing = (followObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [followObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        if (followObj.countFlag === false) {
          sql.query(
            `SELECT u.* FROM followers as f INNER JOIN users as u ON f.requestSentTo = u.id WHERE f.requestSentBy = ? && f.requestStatus = ?`,
            [res[0].id, followObj.requestStatus],
            (err, res) => {
              if (err) {
                console.log("error : ", err);
                result(err, null);
                return;
              }
              result(null, res);
              return;
            }
          );
        } else {
          sql.query(
            `SELECT count(f.id) as count FROM followers as f INNER JOIN users as u ON f.requestSentTo = u.id WHERE f.requestSentBy = ? && f.requestStatus = ?`,
            [res[0].id, followObj.requestStatus],
            (err, res) => {
              if (err) {
                console.log("error : ", err);
                result(err, null);
                return;
              }
              result(null, res);
              return;
            }
          );
        }
      } else {
        result({ kind: "request_sent_to_not_match" }, null);
        return;
      }
    }
  );
};

User.requestAction = (requestObj, result) => {
  console.log("this is request", requestObj);
  console.log("this is resss", result);

  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [requestObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        console.log("this is resula wnekmd", result);
        return;
      }
      if (res.length) {
        const loggedInUser = res[0].id;

        sql.query(
          `SELECT * FROM users WHERE id = ?`,
          [requestObj.requestSentBy],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              sql.query(
                "SELECT * FROM followers WHERE requestSentBy = ? && requestSentTo = ? && requestStatus = ?",
                [requestObj.requestSentBy, loggedInUser, "Pending"],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  if (res.length) {
                    // debugger;

                    if (
                      requestObj.requestStatus == "Declined" ||
                      requestObj.requestStatus == "Unfollowed"
                    ) {
                      sql.query(
                        "DELETE FROM followers WHERE requestSentBy = ? && requestSentTo = ?",
                        [requestObj.requestSentBy, loggedInUser],
                        (err, res) => {
                          if (err) {
                            console.log("error : ", err);
                            result(err, null);
                            return;
                          }
                          result.query_status = "request_update_success";
                          result(null, result);
                        }
                      );
                      console.log(
                        "this is the status code of ",
                        requestObj.requestStatus
                      );
                    } else {
                      sql.query(
                        "UPDATE followers SET requestStatus = ?, requestStatusDate = ? WHERE requestSentBy = ? && requestSentTo = ?",
                        [
                          requestObj.requestStatus,
                          requestObj.requestStatusDate,
                          requestObj.requestSentBy,
                          loggedInUser,
                        ],
                        (err, res) => {
                          if (err) {
                            console.log("error : ", err);
                            result(err, null);
                            return;
                          }
                          result.query_status = "request_update_success";
                          result(null, result);
                        }
                      );
                      console.log(
                        "this is the status code of ",
                        requestObj.requestStatus
                      );
                    }
                  } else {
                    result({ kind: "request_not_exist" }, null);
                  }
                }
              );
            } else {
              result({ kind: "request_sent_to_not_match" }, null);
              return;
            }
          }
        );
      } else {
        result({ kind: "request_sent_by_not_match" }, null);
        return;
      }
    }
  );
};

User.deleteRequest = (follow, result) => {
  sql.query(`SELECT * FROM users WHERE uuId = ?`, [follow.uuId], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      const loggedInUserId = res[0].id;

      sql.query(
        `SELECT * FROM users WHERE id = ?`,
        [follow.requestSentTo],
        (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
          if (res.length) {
            sql.query(
              "SELECT * FROM followers WHERE requestSentBy = ? && requestSentTo = ?",
              [loggedInUserId, follow.requestSentTo],
              (err, res) => {
                if (err) {
                  console.log("error : ", err);
                  result(err, null);
                  return;
                }
                if (res.length) {
                  if (res[0].requestStatus == "Pending") {
                    sql.query(
                      "DELETE FROM followers WHERE requestSentBy = ? && requestSentTo = ?",
                      [loggedInUserId, follow.requestSentTo],
                      (err, res) => {
                        if (err) {
                          console.log("error : ", err);
                          result(err, null);
                          return;
                        }
                        follow.query_status = "delete_request_sent_success";
                        result(null, follow);
                      }
                    );
                  } else {
                    result({ kind: "request_delete_to_not_match" }, null);
                    return;
                  }
                } else {
                  result({ kind: "request_delete_to_not_match" }, null);
                  return;
                }
              }
            );
          } else {
            result({ kind: "request_delete_to_not_match" }, null);
            return;
          }
        }
      );
    } else {
      result({ kind: "request_delete_to_not_match" }, null);
      return;
    }
  });
};

User.userDetails = (uuId, result) => {
  sql.query(`SELECT * FROM users WHERE uuId = ?`, [uuId], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      result(null, res);
      return;
    } else {
      result({ kind: "user_not_match" }, null);
      return;
    }
  });
};

User.getUsersList = (searchObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [searchObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        loggedInUserId = res[0].id;
        searchStringWIthLikeOperator = "%" + searchObj.searchUserString + "%";

        //if condition for search in email only as it contains @ else if for name or email
        if (searchObj.searchUserString.includes("@")) {
          queryText =
            "SELECT u.id, u.firstName, u.middleName, u.lastName, 'following' as `followerTypeFlag` FROM `users` as u JOIN `followers` as f ON u.id = f.requestSentTo WHERE u.profileStatus = ? && u.email LIKE ? && u.id <> ? && f.requestSentBy = ?";
          queryText += " UNION ";
          queryText +=
            "SELECT u.id, u.firstName, u.middleName, u.lastName, 'follower'  as `followerTypeFlag` FROM `users` as u JOIN `followers` as f ON u.id = f.requestSentBy WHERE u.profileStatus = ? && u.email LIKE ? && u.id <> ? && f.requestSentTo = ?";
          queryText += " UNION ";
          queryText +=
            "SELECT  u.id, u.firstName, u.middleName, u.lastName, 'none'  as `followerTypeFlag` FROM `users` as u WHERE id not in (SELECT distinct(requestSentTo) FROM `followers` UNION SELECT distinct(requestSentBy) FROM `followers`)";
          queryValues = [
            "true",
            searchStringWIthLikeOperator,
            loggedInUserId,
            loggedInUserId,
            "true",
            searchStringWIthLikeOperator,
            loggedInUserId,
            loggedInUserId,
          ];
          //queryCountText = 'SELECT count(u.id) as count  FROM `users` as u JOIN `followers` as f ON u.id = f.requestSentTo WHERE u.profileStatus = ? && email LIKE ? && u.id <> ? && f.requestSentBy = ?';
          queryCountText =
            "SELECT COUNT(*) as countUser FROM (" + queryText + ") countUser";
        } else {
          queryText =
            "SELECT u.id, u.firstName, u.middleName, u.lastName, 'following' as `followerTypeFlag` FROM `users` as u JOIN `followers` as f ON u.id = f.requestSentTo WHERE u.profileStatus = ? && (u.firstName LIKE ? OR u.middleName LIKE ? OR u.lastName LIKE ? OR u.email LIKE ?) &&  u.id <> ? && f.requestSentBy = ?";
          queryText += " UNION ";
          queryText +=
            "SELECT u.id, u.firstName, u.middleName, u.lastName, 'follower'  as `followerTypeFlag` FROM `users` as u JOIN `followers` as f ON u.id = f.requestSentBy WHERE u.profileStatus = ? && (u.firstName LIKE ? OR u.middleName LIKE ? OR u.lastName LIKE ? OR u.email LIKE ?) && u.id <> ? && f.requestSentTo = ?";
          queryText += " UNION ";
          queryText +=
            "SELECT  u.id, u.firstName, u.middleName, u.lastName, 'none'  as `followerTypeFlag` FROM `users` as u WHERE id not in (SELECT distinct(requestSentTo) FROM `followers` UNION SELECT distinct(requestSentBy) FROM `followers`)";
          queryValues = [
            "true",
            searchStringWIthLikeOperator,
            searchStringWIthLikeOperator,
            searchStringWIthLikeOperator,
            searchStringWIthLikeOperator,
            loggedInUserId,
            loggedInUserId,
            "true",
            searchStringWIthLikeOperator,
            searchStringWIthLikeOperator,
            searchStringWIthLikeOperator,
            searchStringWIthLikeOperator,
            loggedInUserId,
            loggedInUserId,
          ];
          //queryCountText = 'SELECT count(u.id) as count FROM `users` as u JOIN `followers` as f ON u.id = f.requestSentTo WHERE u.profileStatus = ? && (u.firstName LIKE ? OR u.middleName LIKE ? OR u.lastName LIKE ? OR u.email LIKE ?) &&  u.id <> ? && f.requestSentBy = ?';
          queryCountText =
            "SELECT COUNT(*) as countUser FROM (" + queryText + ") countUser";
        }
        //console.log(queryText);console.log(queryValues);return;

        if (searchObj.countFlag === false) {
          sql.query(queryText, queryValues, (err, res) => {
            //console.log('HI');console.log(res);return;
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }

            result(null, res);
            return;
          });
        } else {
          sql.query(queryCountText, queryValues, (err, res) => {
            //console.log('HI');console.log(res);return;
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            result(null, res);
            return;
          });
        }
      } else {
        result({ kind: "user_not_match" }, null);
        return;
      }
    }
  );
};

User.submitTweet = (tweet, result) => {
  sql.query(`SELECT * FROM users WHERE uuId = ?`, [tweet.uuId], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      sql.query(
        "INSERT INTO tweet SET userId = ?, content = ?, createdDate = ?, status = ?",
        [res[0].id, tweet.content, tweet.createdDate, tweet.status],
        (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
          tweet.query_status = "tweet_insert_success";
          result(null, tweet);
          return;
        }
      );
    } else {
      result({ kind: "user_not_found" }, null);
    }
  });
};

User.updateTweet = (tweet, result) => {
  sql.query(`SELECT * FROM users WHERE uuId = ?`, [res[0].id], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      sql.query(
        `SELECT * FROM tweet WHERE id = ?`,
        [tweet.tweetId],
        (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
          if (res.length) {
            sql.query(
              "UPDATE tweet SET content = ?, modifiedDate = ?, status = ? WHERE userId = ? and id = ?",
              [
                tweet.content,
                tweet.modifiedDate,
                tweet.status,
                tweet.uuId,
                tweet.tweetId,
              ],
              (err, res) => {
                if (err) {
                  console.log("error : ", err);
                  result(err, null);
                  return;
                }
                tweet.query_status = "tweet_update_success";
                result(null, tweet);
                return;
              }
            );
          } else {
            result({ kind: "tweet_not_found" }, null);
          }
        }
      );
    } else {
      result({ kind: "user_not_found" }, null);
    }
  });
};

User.getReTweetListOfFollower = (tweetObj, result) => {
    var userIdArray = [];
    sql.query(
        `SELECT * FROM users WHERE uuId = ?`,
        [tweetObj.uuId],
        (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
    
          if (res.length) {
            const loggedInUserId = res[0].id;
            userIdArray.push(loggedInUserId);
            sql.query(
            `SELECT requestSentTo FROM followers WHERE requestSentBy = ? && requestStatus = ?`,
            [loggedInUserId, "Confirmed"],
            (err, res) => {
                if (err) {
                console.log("error : ", err);
                result(err, null);
                return;
                }
                if (res.length) {
                    res.forEach(function (key, index) {
                        userIdArray.push(res[index].requestSentTo);
                    });
                }
                //let userIdArrayStr = userIdArray.join("','");
                //console.log(userIdArrayStr);
                result(null, userIdArray);
                return;
            }
            );
        }
    })
}

User.getTweetList = (tweetObj, result) => {
    
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        const loggedInUserId = res[0].id;
        // let reTweetUserId = "'" + tweetObj.reTweetUserId + "'"
        let reTweetUserId = tweetObj.reTweetUserId
        //console.log(tweetObj);
        
        if (tweetObj.countFlag === false) {
          if (tweetObj.tweetTab == "tweets") {
            queryText =
              "SELECT DISTINCT(t.id), t.userId, t.content, t.countRetweet, t.countLike, t.countComment, t.createdDate, if(l.likeId > 0, 'true', 'false') as likeFlag, u.firstName, u.lastName from `tweet` as t LEFT JOIN `tweetLike` as l ON t.id = l.tweetId LEFT JOIN users as u ON t.userId = u.id WHERE t.status = ? && t.`id` IN (";
            queryText +=
              " SELECT t.id as tweetId FROM `tweet` as t WHERE t.userId IN (?) && t.status = ? ";
            queryText += " UNION ";
            queryText +=
              " SELECT r.tweetId FROM `reTweet` as r WHERE r.userId IN (?)) order by t.createdDate desc";
            queryValues = [
              "Enabled",
              reTweetUserId,
              tweetObj.status,
              reTweetUserId,
            ];
            
          } else if (tweetObj.tweetTab == "tweets_comments") {
            queryText =
              "SELECT DISTINCT(t.id), t.userId, t.content, t.countRetweet, t.countLike, t.countComment, t.createdDate, if(l.likeId > 0, 'true', 'false') as likeFlag, u.firstName, u.lastName from `tweet` as t LEFT JOIN `tweetLike` as l ON t.id = l.tweetId LEFT JOIN users as u ON t.userId = u.id WHERE t.status = ? && t.`id` IN (SELECT t.id as tweetId FROM `tweet` as t WHERE t.userId = ? && status = ? UNION SELECT c.tweetId FROM `comments` as c WHERE c.userId = ?) order by t.createdDate desc";
            queryValues = [
              "Enabled",
              loggedInUserId,
              tweetObj.status,
              loggedInUserId,
            ];
          } else if (tweetObj.tweetTab == "tweets_like") {
            queryText =
              "SELECT DISTINCT(t.id), t.userId, t.content, t.countRetweet, t.countLike, t.countComment, t.createdDate, if(l.likeId > 0, 'true', 'false') as likeFlag, u.firstName, u.lastName from `tweet` as t LEFT JOIN `tweetLike` as l ON t.id = l.tweetId LEFT JOIN users as u ON t.userId = u.id WHERE t.status = ? && t.`id` IN (SELECT l.tweetId FROM `tweetlike` as l WHERE l.userId = ?) order by t.createdDate desc";
            queryValues = ["Enabled", loggedInUserId];
          }
        } else {
          if (tweetObj.tweetTab == "tweets") {
            queryText =
              "SELECT count(DISTINCT(t.id)) as count from `tweet` as t LEFT JOIN `tweetLike` as l ON t.id = l.tweetId LEFT JOIN users as u ON t.userId = u.id WHERE t.status = ? && t.`id` IN (";
            queryText +=
              " SELECT t.id as tweetId FROM `tweet` as t WHERE t.userId IN (?) && t.status = ? ";
            queryText += " UNION ";
            queryText +=
              " SELECT r.tweetId FROM `reTweet` as r WHERE r.userId IN (?)) ";
            queryValues = [
              "Enabled",
              reTweetUserId,
              tweetObj.status,
              reTweetUserId,
            ];
          } else if (tweetObj.tweetTab == "tweets_comments") {
            queryText =
              "SELECT count(DISTINCT(t.id)) as count from `tweet` as t LEFT JOIN `tweetLike` as l ON t.id = l.tweetId WHERE t.status = ? && t.`id` IN (SELECT t.id as tweetId FROM `tweet` as t WHERE t.userId = ? && status = ? UNION SELECT c.tweetId FROM `comments` as c WHERE c.userId = ?)";
            queryValues = [
              "Enabled",
              loggedInUserId,
              tweetObj.status,
              loggedInUserId,
            ];
          } else if (tweetObj.tweetTab == "tweets_like") {
            queryText =
              "SELECT count(DISTINCT(t.id)) as count from `tweet` as t LEFT JOIN `tweetLike` as l ON t.id = l.tweetId WHERE t.status = ? && t.`id` IN (SELECT l.tweetId FROM `tweetlike` as l WHERE l.userId = ?)";
            queryValues = ["Enabled", loggedInUserId];
          }
        }
        
        c = sql.query(queryText, queryValues, (err, res) => {
            console.log(c.sql);
//console.log('This is response', res);
//console.log('This is error', err);
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
          if (res.length >= 0) {
            
            result(null, res);
            return;
          } else {
            result({ kind: "tweet_not_found" }, null);
            //result(null, {});
            return;
          }
        });
      } else {
        result({ kind: "user_not_match" }, null);
        return;
      }
    }
  );
};

User.deleteTweet = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        const loggedInUserId = res[0].id;

        sql.query(
          `SELECT * FROM tweet WHERE id = ?`,
          [tweetObj.tweetId],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              sql.query(
                "DELETE FROM tweet WHERE id = ? && userId = ?",
                [tweetObj.tweetId, loggedInUserId],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  tweetObj.query_status = "tweet_delete_success";
                  result(null, tweetObj);
                  return;
                }
              );
            } else {
              result({ kind: "tweet_not_found" }, null);
              return;
            }
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
        return;
      }
    }
  );
};

User.submitTweetComment = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        const loggedInUserId = res[0].id;
        //console.log("SELECT * FROM tweet WHERE userId = ? && id = ?", res[0].id, tweet.tweetId);

        sql.query(
          "SELECT * FROM tweet WHERE id = ?",
          [tweetObj.tweetId],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              sql.query(
                "INSERT INTO comments SET tweetId = ?, userId = ?, comment = ?, commentParentId = ?, commentStatus = ?, createdDate = ? ",
                [
                  tweetObj.tweetId,
                  loggedInUserId,
                  tweetObj.comment,
                  tweetObj.commentParentId,
                  tweetObj.commentStatus,
                  tweetObj.createdDate,
                ],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  sql.query(
                    "SELECT countComment FROM tweet WHERE id = ?",
                    [tweetObj.tweetId],
                    (err, res) => {
                      if (res.length) {
                        countComment = res[0].countComment + 1;
                        sql.query(
                          "UPDATE tweet SET countComment = ? WHERE id = ?",
                          [countComment, tweetObj.tweetId],
                          (err, res) => {
                            if (err) {
                              console.log("error : ", err);
                              result(err, null);
                              return;
                            }
                          }
                        );
                      }
                    }
                  );
                  tweetObj.query_status = "tweet_comment_insert_success";
                  result(null, tweetObj);
                  return;
                }
              );
            } else {
              result({ kind: "tweet_not_found" }, null);
            }
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
      }
    }
  );
};

User.getTweetCommentList = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        const loggedInUserId = res[0].id;

        sql.query(
          `SELECT * FROM tweet WHERE userId = ? && id = ?`,
          [loggedInUserId, tweetObj.tweetId],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              if (tweetObj.countFlag === false) {
                queryText =
                  "SELECT commentId, tweetId, comment, commentParentId, commentStatus, createdDate FROM comments WHERE tweetId = ? && commentStatus = ? && commentParentId = ?";
                queryValues = [
                  res[0].id,
                  tweetObj.commentStatus,
                  tweetObj.commentParentId,
                ];
              } else {
                queryText =
                  "SELECT count(commentId) as count FROM comments WHERE tweetId = ? && commentStatus = ? && commentParentId = ?";
                queryValues = [
                  res[0].id,
                  tweetObj.commentStatus,
                  tweetObj.commentParentId,
                ];
              }

              sql.query(queryText, queryValues, (err, res) => {
                if (err) {
                  console.log("error : ", err);
                  result(err, null);
                  return;
                }
                if (res.length) {
                  result(null, res);
                  return;
                } else {
                  result({ kind: "tweet_not_found" }, null);
                  return;
                }
              });
            } else {
              result({ kind: "tweet_not_found" }, null);
            }
          }
        );
      } else {
        result({ kind: "user_not_match" }, null);
        return;
      }
    }
  );
};

User.submitTweetLike = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        const loggedInUserId = res[0].id;
        sql.query(
          "SELECT * FROM tweet WHERE userId = ? && id = ?",
          [res[0].id, tweetObj.tweetId],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              sql.query(
                "INSERT INTO tweetLike SET tweetId = ?, userId = ?, likeStatus = ?, createdDate = ? ",
                [
                  tweetObj.tweetId,
                  loggedInUserId,
                  "Like",
                  tweetObj.createdDate,
                ],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  sql.query(
                    "SELECT countLike FROM tweet WHERE id = ?",
                    [tweetObj.tweetId],
                    (err, res) => {
                      if (res.length) {
                        countLike = res[0].countLike + 1;
                        sql.query(
                          "UPDATE tweet SET countLike = ? WHERE id = ?",
                          [countLike, tweetObj.tweetId],
                          (err, res) => {
                            if (err) {
                              console.log("error : ", err);
                              result(err, null);
                              return;
                            }
                          }
                        );
                      }
                    }
                  );
                  tweetObj.query_status = "tweet_like_insert_success";
                  result(null, tweetObj);
                  return;
                }
              );
            } else {
              result({ kind: "tweet_not_found" }, null);
            }
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
      }
    }
  );
};

User.getTweetLikeList = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        const loggedInUserId = res[0].id;

        sql.query(
          `SELECT * FROM tweet WHERE id = ?`,
          [tweetObj.tweetId],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              if (tweetObj.countFlag === false) {
                queryText =
                  "SELECT likeId, tweetId, userId, createdDate FROM tweetLike WHERE tweetId = ? && likeStatus = ?";
                queryValues = [tweetObj.tweetId, "Like"];
              } else {
                queryText =
                  "SELECT count(likeId) as count FROM tweetLike WHERE tweetId = ? && likeStatus = ?";
                queryValues = [tweetObj.tweetId, "Like"];
              }

              sql.query(queryText, queryValues, (err, res) => {
                if (err) {
                  console.log("error : ", err);
                  result(err, null);
                  return;
                }
                if (res.length) {
                  result(null, res);
                  return;
                } else {
                  result({ kind: "tweet_not_found" }, null);
                  return;
                }
              });
            } else {
              result({ kind: "tweet_not_found" }, null);
            }
          }
        );
      } else {
        result({ kind: "user_not_match" }, null);
        return;
      }
    }
  );
};

User.submitCommentLike = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        const loggedInUserId = res[0].id;
        sql.query(
          "SELECT * FROM comments WHERE commentId = ? && tweetId = ?",
          [tweetObj.commentId, tweetObj.tweetId],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              sql.query(
                "INSERT INTO commentLike SET commentId = ?, userId = ?, likeStatus = ?, createdDate = ? ",
                [
                  tweetObj.commentId,
                  loggedInUserId,
                  "Like",
                  tweetObj.createdDate,
                ],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  tweetObj.query_status = "comment_like_insert_success";
                  result(null, tweetObj);
                  return;
                }
              );
            } else {
              result({ kind: "comment_not_found" }, null);
            }
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
      }
    }
  );
};

User.getCommentLikeList = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        const loggedInUserId = res[0].id;

        sql.query(
          `SELECT * FROM comments WHERE commentId = ? && tweetId = ?`,
          [tweetObj.commentId, tweetObj.tweetId],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              if (tweetObj.countFlag === false) {
                queryText =
                  "SELECT likeId, commentId, userId, likeStatus, createdDate FROM commentLike WHERE commentId = ? && likeStatus = ?";
                queryValues = [tweetObj.commentId, "Like"];
              } else {
                queryText =
                  "SELECT count(likeId) as count FROM commentLike WHERE commentId = ? && likeStatus = ?";
                queryValues = [tweetObj.commentId, "Like"];
              }

              sql.query(queryText, queryValues, (err, res) => {
                if (err) {
                  console.log("error : ", err);
                  result(err, null);
                  return;
                }
                if (res.length) {
                  result(null, res);
                  return;
                } else {
                  result({ kind: "tweet_not_found" }, null);
                  return;
                }
              });
            } else {
              result({ kind: "tweet_not_found" }, null);
            }
          }
        );
      } else {
        result({ kind: "user_not_match" }, null);
        return;
      }
    }
  );
};

User.deleteTweetLike = (tweetObj, result) => {
  console.log(tweetObj, result, "this hit bye api");
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        console.log(res, "this hit bye api res");
        const loggedInUserId = res[0].id;
        sql.query(
          "SELECT * FROM tweetLike WHERE tweetId = ?",
          [tweetObj.tweetId],
          (err, res) => {
            if (err) {
              console.log(res, "this hit bye api res1");
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              console.log(res, "this hit bye api re2");
              console.log(
                tweetObj.tweetId,
                loggedInUserId,
                "this hit bye api re2"
              );
              sql.query(
                "DELETE FROM tweetLike WHERE tweetId = ? && userId = ?",
                [tweetObj.tweetId, loggedInUserId],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  sql.query(
                    "SELECT countLike FROM tweet WHERE id = ?",
                    [tweetObj.tweetId],
                    (err, res) => {
                      if (err) {
                        console.log(res, "this hit bye api res3");
                        console.log("error : ", err);
                        result(err, null);
                        return;
                      }
                      if (res.length) {
                        console.log(res, "this hit bye api res4");
                        let updateCountLike = res[0].countLike - 1;
                        console.log(
                          "UPDATE tweet SET countLike = ? WHERE id = ?",
                          updateCountLike,
                          tweetObj.tweetId
                        );
                        sql.query(
                          "UPDATE tweet SET countLike = ? WHERE id = ?",
                          [updateCountLike, tweetObj.tweetId],
                          (err, res) => {
                            if (err) {
                              console.log("error : ", err);
                              result(err, null);
                              return;
                            }
                          }
                        );
                      }
                    }
                  );
                  tweetObj.query_status = "tweet_unlike_success";
                  result(null, tweetObj);
                  console.log(tweetObj);
                  return;
                }
              );
            } else {
              result({ kind: "tweet_not_found" }, null);
            }
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
      }
    }
  );
};

User.deleteCommentLike = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        const loggedInUserId = res[0].id;

        sql.query(
          "SELECT * FROM commentLike WHERE commentId = ? && userId = ?",
          [tweetObj.commentId, loggedInUserId],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              sql.query(
                "DELETE FROM commentLike WHERE userId = ? && commentId = ?",
                [loggedInUserId, tweetObj.commentId],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    console.log("this is error ", err);
                    return;
                  }
                  tweetObj.query_status = "tweet_unlike_success";
                  result(null, tweetObj);
                  return;
                }
              );
            } else {
              result({ kind: "tweet_not_found" }, null);
            }
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
      }
    }
  );
};

User.submitReTweet = (tweet, result) => {
  sql.query(`SELECT * FROM users WHERE uuId = ?`, [tweet.uuId], (err, res) => {
    if (err) {
      console.log("error : ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      sql.query(
        "INSERT INTO retweet SET userId = ?, tweetId = ?, createdDate = ?",
        [res[0].id, tweet.tweetId, tweet.createdDate],
        (err, res) => {
          if (err) {
            console.log("error : ", err);
            result(err, null);
            return;
          }
          sql.query(
            "SELECT countRetweet FROM tweet WHERE id = ?",
            [tweet.tweetId],
            (err, res) => {
              if (res.length) {
                countReTweet = res[0].countRetweet + 1;
                sql.query(
                  "UPDATE tweet SET countReTweet = ? WHERE id = ?",
                  [countReTweet, tweet.tweetId],
                  (err, res) => {
                    if (err) {
                      console.log("error : ", err);
                      result(err, null);
                      return;
                    }
                  }
                );
              }
            }
          );
          tweet.query_status = "retweet_insert_success";
          result(null, tweet);
          return;
        }
      );
    } else {
      result({ kind: "user_not_found" }, null);
    }
  });
};

User.deleteReTweet = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        const loggedInUserId = res[0].id;
        sql.query(
          `SELECT * FROM tweet WHERE id = ?`,
          [tweetObj.tweetId],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              sql.query(
                `SELECT * FROM tweet WHERE id = ?`,
                [tweetObj.tweetId],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  if (res.length) {
                    sql.query(
                      `SELECT * FROM reTweet WHERE tweetId = ? && userId = ?`,
                      [tweetObj.tweetId, loggedInUserId],
                      (err, res) => {
                        if (err) {
                          console.log("error : ", err);
                          result(err, null);
                          return;
                        }
                        if (res.length) {
                          sql.query(
                            "DELETE FROM reTweet WHERE tweetId = ? && userId = ?",
                            [tweetObj.tweetId, loggedInUserId],
                            (err, res) => {
                              if (err) {
                                console.log("error : ", err);
                                result(err, null);
                                return;
                              }
                              sql.query(
                                "SELECT countRetweet FROM tweet WHERE id = ?",
                                [tweetObj.tweetId],
                                (err, res) => {
                                  if (res.length) {
                                    countReTweet = res[0].countRetweet - 1;
                                    sql.query(
                                      "UPDATE tweet SET countReTweet = ? WHERE id = ?",
                                      [countReTweet, tweetObj.tweetId],
                                      (err, res) => {
                                        if (err) {
                                          console.log("error : ", err);
                                          result(err, null);
                                          return;
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                              tweetObj.query_status = "retweet_delete_success";
                              result(null, tweetObj);
                              return;
                            }
                          );
                        } else {
                          result({ kind: "retweet_not_found" }, null);
                          return;
                        }
                      }
                    );
                  } else {
                    result({ kind: "retweet_not_found" }, null);
                    return;
                  }
                }
              );
            } else {
              result({ kind: "tweet_not_found" }, null);
              return;
            }
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
        return;
      }
    }
  );
};

User.detailTweet = (tweetObj, result) => {
  sql.query(
    `SELECT * FROM users WHERE uuId = ?`,
    [tweetObj.uuId],
    (err, res) => {
      if (err) {
        console.log("error : ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        const loggedInUserId = res[0].id;
        let tweetDetailObj = {};

        sql.query(
          `SELECT t.*, u.uuId, CONCAT(u.firstName, ' ',  u.middleName, ' ', u.lastName) as userName FROM tweet as t LEFT JOIN users as u ON t.userId = u.id WHERE t.id = ? && t.status = ?`,
          [tweetObj.tweetId, "Enabled"],
          (err, res) => {
            if (err) {
              console.log("error : ", err);
              result(err, null);
              return;
            }
            if (res.length) {
              let tweetDetail = res[0];
              sql.query(
                `SELECT t.userId, u.uuId, CONCAT(u.firstName, ' ', u.middleName, ' ', u.lastName) as name FROM tweetLike as t LEFT JOIN users as u ON t.userId = u.id WHERE t.tweetId = ? `,
                [tweetObj.tweetId],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  if (res.length) {
                    debugger;
                    console.log("this is like  user list ", res);
                    tweetDetail.likeUsersList = res;
                  } else {
                    tweetDetail.likeUsersList = [];
                  }
                }
              );

              sql.query(
                `SELECT c.commentId, c.userId, c.comment, u.uuId, CONCAT(u.firstName, ' ', u.middleName, ' ', u.lastName) as name FROM comments as c LEFT JOIN users as u ON c.userId = u.id WHERE c.tweetId = ? && c.commentParentId = ?`,
                [tweetObj.tweetId, "0"],
                (err, res) => {
                  if (err) {
                    console.log("error : ", err);
                    result(err, null);
                    return;
                  }
                  if (res.length) {
                    tweetDetail.commentList = res;
                    console.log("this is like  user list ", res);
                  } else {
                    tweetDetail.commentList = [];
                  }
                }
              );

              setTimeout(function () {
                //console.log(tweetDetail);
                tweetDetailObj[0] = tweetDetail;
                result(null, tweetDetailObj);
                return;
              }, 1000);
            } else {
              result({ kind: "tweet_not_found" }, null);
              return;
            }
          }
        );
      } else {
        result({ kind: "user_not_found" }, null);
        return;
      }
    }
  );
};

module.exports = User;

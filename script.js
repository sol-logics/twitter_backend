const express = require('express');
var jwt = require('jsonwebtoken');
const cors = require('cors');

const bodyparser = require('body-parser');
const routeUrl = require('./app/routes/user.route.js');
const dotenv = require('dotenv');
const jsonwebtoken = require('jsonwebtoken');

var app = express();

//setup global configuration access
dotenv.config();

app.use(cors({
    origin: 'http://localhost:8080/'
}));
//configuring express server
app.use(bodyparser.json());
app.use('/api/', routeUrl);



const port = process.env.PORT || 8080 ;
// const port = process.env.PORT || 3000 ;
app.listen(port, () => console.log(`Listening on port ${port}..`));